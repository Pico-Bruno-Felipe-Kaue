#include "node.h"
#include "symbol_table.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

extern int lineno;

char *node_lexema[lexpr_node + 1] = {
	[program_node] = "code",
	[decs_node] = "declaracoes",
	[decl_node] = "declaracao",
	[decl_list_node] = "lista_declaracao",
	[listad_node] = "lista_dupla",
	[bloc_node] = "lista_acoes",
	[com_node] = "atribuicao",
	[lvalue_node] = "valor_indexado",
	[lexpr_node] = "lista_expr",
	[plus_node] = "+",
	[minus_node] = "-",
	[mult_node] = "*",
	[div_node] = "/",
	[proc_node] = "chamada_funcao",
	[if_node] = "if",
	[while_node] = "while",
	[and_node] = "and",
	[or_node] = "or",
	[not_node] = "not",
	[sup_node] = ">",
	[inf_node] = "<",
	[inf_eq_node] = "le",
	[sup_eq_node] = "ge",
	[eq_node] = "eq",
	[neq_node] = "!="
};

void print_tree(Node *n, int depth)
{
	int i;
	if(!n) return; /* acontece quando o if não them else, por exemplo */
	for(i = 0; i < depth; i++)
		printf(" ");
	//printf("nl %i id %i l %s t %i a %p nc %i\n",	n->num_line, n->id, n->lexeme, n->type, n->attribute, n->num_children);
	//printf("%s | %i | %i\n",n->lexeme, n->type, n->num_children);
	printf("%s\n", n->lexeme);
	switch(n->type) {
		case program_node:
			printf("--------------------------------- symbol_table----------------------------------------\n");
			print_table(n->attribute);
			printf("--------------------------------------------------------------------------------------\n");
			break;
		case idf_node:
			if(n->attribute) {
				printf("--------------------------------- symbol_table_entry ---------------------------------\n");
				print_table_entry(n->attribute);
				printf("--------------------------------------------------------------------------------------\n");
			}
			break;
		case int_node:
			for(i = 0; i < depth + 1; i++) printf(" ");
			printf("-valor: %i\n", *(int *)n->attribute);
			break;
		case float_node:
			for(i = 0; i < depth + 1; i++) printf(" ");
			printf("-valor: %f\n", *(float *)n->attribute);
			break;
	}
	for(i = 0; i < n->num_children; i++)
		print_tree(n->child[i], depth + 1);
}

/* Variavel que indica qual o proximo id valido */
static int __nodes_ids__ = 0;

Node* vcreate_node(Node_type t, char* lexema, 
                  void* att, int nbc, va_list *nodes);

Node* create_node(Node_type t, 
                  int nbc, ...) {
	va_list nodes;
	va_start(nodes,nbc);
	Node *node = vcreate_node(t, node_lexema[t], NULL, nbc, &nodes);
	va_end(nodes);
	return node;
}

Node* create_node_with_att(Node_type t, 
                  void* att, int nbc, ...) {
	va_list nodes;
	va_start(nodes,nbc);
	Node *node = vcreate_node(t, node_lexema[t], att, nbc, &nodes);
	va_end(nodes);
	return node;
}

Node* create_node_with_lexema(Node_type t, char* lexema, 
                  int nbc, ...) {
	va_list nodes;
	va_start(nodes,nbc);
	Node *node = vcreate_node(t, lexema, NULL, nbc, &nodes);
	va_end(nodes);
	return node;
}

Node* vcreate_node(Node_type t, char* lexema, 
                  void* att, int nbc, va_list *nodes) {
	int i;
	assert(t >= program_node && t <= lexpr_node && nbc >= 0 && nbc <= MAX_CHILDREN_NUMBER);
  Node* node = malloc(sizeof(Node));
	assert(node);
	node->num_line = lineno;
	node->type = t;
	node->lexeme = strdup(lexema);
	node->attribute = att;
	node->num_children = nbc;
	for(i = 0; i < nbc; i++)
		node->child[i] = va_arg(*nodes, Node* );
	node->id = __nodes_ids__++;
	return node;
}

Node* create_leaf(Node_type t, char* lex) {
  return create_node_with_lexema(t, lex, 0, NULL);
}

Node* create_leaf_with_att(Node_type t, char* lex, void *att) {
  return vcreate_node(t, lex, att, 0, NULL);
}

int nb_of_children(Node* n) {
	assert(n);
	return n->num_children;
}


Node* child(Node* n, int i) {
  assert(n && i > 0 && i < n->num_children);
	return n->child[i];
}

int deep_free_node(Node* n) {
	int i;
	if(n) {
#ifdef __BFK_DO_TESTING__ // if we are testing
		printf("deleting...\n");
		print_tree(n, 0);
#endif
		for(i = 0; i < n->num_children; i++)
			deep_free_node(n->child[i]);
		free(n);
	}
	return 0;
}

int height(Node *n) {
	int max = 0, i, temp;
	assert(n);
	for(i = 0; i < n->num_children; i++) {
		temp = height(n->child[i]);
		if(temp > max)
			max = temp;
	}
	return 1 + max;
}

int pack_nodes(Node*** array_of_nodes, const int cur_size, Node* n) {
  assert(cur_size >= 0 && cur_size < MAX_CHILDREN_NUMBER && n);
	if(!cur_size) {
		*array_of_nodes = (Node **) malloc(sizeof(Node *) * MAX_CHILDREN_NUMBER);
		assert(*array_of_nodes);
	}
	array_of_nodes[0][cur_size] = n;
	return cur_size + 1;
}

